# Python 3.7 or better. Uses SciPy/NumPy/Matplotlib Works in Anaconda distro
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import probscale as prb #see: https://matplotlib.org/mpl-probscale/api/probscale.html
# see also: https://matplotlib.org/mpl-probscale/tutorial/closer_look_at_viz.html

infile_name= "crushville_2019_combined.csv"


df = pd.read_csv(infile_name)
print(df)

fig = plt.figure(1)
#fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')

data_raw = df["Total"]
prb.probplot(df["Total"], ax=ax, probax='y', color='k')
plt.xlabel("Total Score")
plt.ylabel("Percentile")
plt.title("Overall Population")
plt.savefig("fig_1_overall.png", width="4in", height="3in", dpi=300)



fig = plt.figure(10)
#fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
prb.probplot(df["Max Score"], ax=ax, probax='y', color='k')
plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Overall Population")
plt.savefig("fig_10_max.png", width="4in", height="3in", dpi=300)




fig = plt.figure(2)
#fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
men = df[df.Gender == "M"]
women = df[df.Gender == "F"]
data_raw = df["Total"]
prb.probplot(men["Total"], ax=ax, probax='y', color='b')
prb.probplot(women["Total"], ax=ax, probax='y', color='r')#, color=col)
plt.xlabel("Total Score")
plt.ylabel("Percentile")
plt.title("Men vs Women")

plt.xlim((0, 36000))
plt.ylim((0.2, 99.8))
plt.savefig("fig_2_men_vs_women.png", width="4in", height="3in", dpi=300)



fig = plt.figure(3)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
men = df[df.Gender == "M"]

men_rec = men[men.League == "Recreational"]
men_beg = men[men.League == "Beginner"]
men_int = men[men.League == "Intermediate"]
men_adv = men[men.League == "Advanced"]
men_mas = men[men.League == "Masters"]

prb.probplot(men_rec["Total"], ax=ax, probax='y', color='g')
prb.probplot(men_beg["Total"], ax=ax, probax='y', color='b')
prb.probplot(men_int["Total"], ax=ax, probax='y', color=(1, 0.75, 0.25))
prb.probplot(men_adv["Total"], ax=ax, probax='y', color='r')
prb.probplot(men_mas["Total"], ax=ax, probax='y', color=(0.75, 0, 1))

plt.ylim((0.5, 99.5))

plt.xlabel("Total Score")
plt.ylabel("Percentile")
plt.title("Men: Scores by League")
plt.savefig("fig_3_men_by_league.png", width="4in", height="3in", dpi=300)



fig = plt.figure(4)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
men = df[df.Gender == "M"]

women_rec = women[women.League == "Recreational"]
women_beg = women[women.League == "Beginner"]
women_int = women[women.League == "Intermediate"]
women_adv = women[women.League == "Advanced"]
women_mas = women[women.League == "Masters"]

prb.probplot(women_rec["Total"], ax=ax, probax='y', color='g')
prb.probplot(women_beg["Total"], ax=ax, probax='y', color='b')
prb.probplot(women_int["Total"], ax=ax, probax='y', color=(1, 0.75, 0.25))
prb.probplot(women_adv["Total"], ax=ax, probax='y', color='r')
prb.probplot(women_mas["Total"], ax=ax, probax='y', color=(0.75, 0, 1))

plt.ylim((1, 99))

plt.xlabel("Total Score")
plt.ylabel("Percentile")
plt.title("Women: Scores by League")
plt.savefig("fig_4_women_by_league.png", width="4in", height="3in", dpi=300)


#%%
fig = plt.figure(5)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')

prb.probplot(df["Max Score"], ax=ax, probax='y', color='k')
plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Max Score: Overall Population")
plt.savefig("fig_5_max_score_all.png", width="4in", height="3in", dpi=300)



fig = plt.figure(6)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
men = df[df.Gender == "M"]
women = df[df.Gender == "F"]

prb.probplot(men["Max Score"], ax=ax, probax='y', color='b')
prb.probplot(women["Max Score"], ax=ax, probax='y', color='r')#, color=col)
plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Max Score: Men vs Women")
plt.savefig("fig_6_max_score_men_vs_women.png", width="4in", height="3in", dpi=300)



fig = plt.figure(7)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
men = df[df.Gender == "M"]

men_rec = men[men.League == "Recreational"]
men_beg = men[men.League == "Beginner"]
men_int = men[men.League == "Intermediate"]
men_adv = men[men.League == "Advanced"]
men_mas = men[men.League == "Masters"]

prb.probplot(men_rec["Max Score"], ax=ax, probax='y', color='g')
prb.probplot(men_beg["Max Score"], ax=ax, probax='y', color='b')
prb.probplot(men_int["Max Score"], ax=ax, probax='y', color=(1, 0.75, 0.25))
prb.probplot(men_adv["Max Score"], ax=ax, probax='y', color='r')
prb.probplot(men_mas["Max Score"], ax=ax, probax='y', color=(0.75, 0, 1))

plt.ylim((0.5, 99.5))
plt.xlim((0, 8000))

plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Max Score: Men by League")
plt.savefig("fig_7_max_score_men_by_league.png", width="4in", height="3in", dpi=300)



fig = plt.figure(8)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
men = df[df.Gender == "M"]

women_rec = women[women.League == "Recreational"]
women_beg = women[women.League == "Beginner"]
women_int = women[women.League == "Intermediate"]
women_adv = women[women.League == "Advanced"]
women_mas = women[women.League == "Masters"]

prb.probplot(women_rec["Max Score"], ax=ax, probax='y', color='g')
prb.probplot(women_beg["Max Score"], ax=ax, probax='y', color='b')
prb.probplot(women_int["Max Score"], ax=ax, probax='y', color=(1, 0.75, 0.25))
prb.probplot(women_adv["Max Score"], ax=ax, probax='y', color='r')
prb.probplot(women_mas["Max Score"], ax=ax, probax='y', color=(0.75, 0, 1))

plt.ylim((0.5, 99.5))
plt.xlim((0, 8000))

plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Max Score: Women by League") 
plt.savefig("fig_8_max_score_women_by_league.png", width="4in", height="3in", dpi=300)

#%%
fig = plt.figure(9)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
plt.scatter(df["Max Score"], df["Flashes from top 5"], color='b', alpha=0.4)
plt.xlabel("Max Score")
plt.ylabel("Flashes")
plt.savefig("fig_9_flashes_vs_max.png", width="4in", height="3in", dpi=300)


#%%
fig = plt.figure(11)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')
atlanta = df[df.Gym == "A"]
kennesaw = df[df.Gym == "K"]
midtown = df[df.Gym == "M"]

prb.probplot(atlanta["Max Score"], ax=ax, probax='y', color='b')
prb.probplot(kennesaw["Max Score"], ax=ax, probax='y', color='r')
prb.probplot(midtown["Max Score"], ax=ax, probax='y', color='g')
plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Max Score by Gym")
plt.ylim((0.5, 99.5))
plt.xlim((0, 8000))

plt.savefig("fig_11_max_score_by_gym.png", width="4in", height="3in", dpi=300)



fig = plt.figure(12)
fig.clf()
ax = plt.gca()
ax.grid(b=True, which='both')

df_rec = df[df.League == "Recreational"]
df_beg = df[df.League == "Beginner"]
df_int = df[df.League == "Intermediate"]
df_adv = df[df.League == "Advanced"]
df_mas = df[df.League == "Masters"]

prb.probplot(df_rec["Max Score"], ax=ax, probax='y', color='g')
prb.probplot(df_beg["Max Score"], ax=ax, probax='y', color='b')
prb.probplot(df_int["Max Score"], ax=ax, probax='y', color=(1, 0.75, 0.25))
prb.probplot(df_adv["Max Score"], ax=ax, probax='y', color='r')
prb.probplot(df_mas["Max Score"], ax=ax, probax='y', color=(0.75, 0, 1))

plt.ylim((0.5, 99.5))
plt.xlim((0, 8000))

plt.xlabel("Max Score")
plt.ylabel("Percentile")
plt.title("Max Score by League")
plt.savefig("fig_12_max_score_all_by_league.png", width="4in", height="3in", dpi=300)